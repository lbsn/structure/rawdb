# Changelog

<!--next-version-placeholder-->

## v1.6.2 (2023-05-15)
### Fix
* Improve naming for topic_group attribute (protobuf upstream) ([`2f93acc`](https://gitlab.com/lbsn/structure/rawdb/-/commit/2f93acc80a930b8bbea821e492c7a7f02643612a))
