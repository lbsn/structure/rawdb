/*
 * Spatial Facet of the Common LBSN Data Structure
 *
 */
 
/* Geo information hierarchy
 * Country Scale -> City Scale -> Place Scale -> Post latlng,
 * Specify highest available geoaccuracy in post_geoaccuracy,
 * either "latlng", "place", "city" or "country"
 * Reference different services should through origin_id
 * "country", "city" and "place" references are not disambiguated
 * between services, for example, there may be a Country ID "1_652136"
 * -> "Germany" (Instagram) and ID 2_675217653 -> "Germany" (Twitter)
 */
/* Create table country
 */
CREATE TABLE spatial. "country" (
    country_guid text,
    origin_id int REFERENCES social. "origin" (origin_id),
    PRIMARY KEY (origin_id, country_guid),
    name text,
    geom_center geometry(Point, 4326),
    geom_area geometry(Polygon, 4326),
    url text,
    name_alternatives text[] NOT NULL DEFAULT '{}'
)
WITH (
    fillfactor = 80
);

COMMENT ON COLUMN spatial. "country".name IS 'Name in English.';

COMMENT ON COLUMN spatial. "country".name_alternatives IS 'Alternative
 names (e.g. in other languages; synonyms).';


/* Create table city
 */
CREATE TABLE spatial. "city" (
    city_guid text,
    origin_id int REFERENCES social. "origin" (origin_id),
    PRIMARY KEY (origin_id, city_guid),
    name text,
    geom_center geometry(Point, 4326),
    geom_area geometry(Polygon, 4326),
    country_guid text,
    FOREIGN KEY (origin_id,
        country_guid) REFERENCES spatial. "country" (origin_id,
        country_guid),
    url text,
    name_alternatives text[] NOT NULL DEFAULT '{}',
    sub_type text
)
WITH (
    fillfactor = 80
);

COMMENT ON COLUMN spatial. "city".name IS 'Name in English.';

COMMENT ON COLUMN spatial. "city".name_alternatives IS 'Alternative
 names (e.g. in other languages; synonyms).';

COMMENT ON COLUMN spatial. "city".sub_type IS 'Subtype for lbsnCity
 (e.g. "Neighborhood", "Admin", etc.).';


/* Create table place
 */
CREATE TABLE spatial. "place" (
    origin_id int REFERENCES social. "origin" (origin_id),
    place_guid text,
    PRIMARY KEY (origin_id, place_guid),
    name text,
    post_count bigint,
    url text,
    geom_center geometry(Point, 4326) DEFAULT ST_GeomFromText ('POINT(0 0)',
        4326),
    geom_area geometry(Polygon, 4326),
    city_guid text,
    FOREIGN KEY (origin_id,
        city_guid) REFERENCES spatial. "city" (origin_id,
        city_guid),
    name_alternatives text[] NOT NULL DEFAULT '{}',
    place_description text,
    place_website text,
    place_phone text,
    address text,
    zip_code text,
    attributes hstore,
    checkin_count bigint,
    like_count bigint,
    parent_places text[],
    modified timestamp DEFAULT transaction_timestamp()
)
WITH (
    fillfactor = 80
);

COMMENT ON COLUMN spatial. "place".name IS 'Name in English.';

COMMENT ON COLUMN spatial. "place".name_alternatives IS 'Alternative
 names (e.g. in other languages; synonyms).';

COMMENT ON COLUMN spatial. "place".attributes IS 'Hstore for any
 additional place attributes (key-value pair).
 Example: category => park; owner => "Katherine Dunn".';

COMMENT ON COLUMN spatial. "place".checkin_count IS 'On some services,
 people can checkin to places (e.g. Foursquare, Facebook),
 indicating that they visited the place.';

COMMENT ON COLUMN spatial. "place".like_count IS 'On some services,
 people can like places (e.g. on Facebook, to get updates etc.),
 reflecting some form of sympathy or interest.';

COMMENT ON COLUMN spatial. "place".address IS 'Full Address,
 if available: street, city, country';

COMMENT ON COLUMN spatial. "place".modified IS 'The latest timestamp
 this rows was updated (defaults to the time of insertion)';

COMMENT ON COLUMN spatial. "place".parent_places IS 'Places can be
 hierarchically structured, list any up-hierarchy places
 parent to this one as guids here';