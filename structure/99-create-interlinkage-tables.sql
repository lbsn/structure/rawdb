/*
 * Interlinkage of the Common LBSN Data Structure
 *
 * Relational tables map one-to-many and many-to-many relationships.
 * Relationships can therefore link entities, e.g. between two different
 * origin_id's (e.g. different services)
 * or between implicit social media and external sources
 * (explicit VGI, e.g. Wikipedia, OSM).
 *
 * Therefore, the following naming conventions are used:  
 *
 * - Names of internal relationships start with an underscore ('_'),
 *   e.g. '_user_mentions_user'
 * - Names of cross-social media relationships start with two underscores ('__'),
 *   e.g. '__users_shareidentity', '__posts_shareidentity'
 * - Names of relationships that link social media and external
 *   sources start with three underscores ('___'),
 *   e.g.  between social me
 */
 
CREATE TABLE interlinkage. "_user_connectsto_user" (
    origin_id int REFERENCES social. "origin" (origin_id),
    user_guid text,
    FOREIGN KEY (origin_id,
        user_guid) REFERENCES social. "user" (origin_id,
        user_guid),
    connectedto_user_guid text,
    FOREIGN KEY (origin_id,
        connectedto_user_guid) REFERENCES social. "user" (origin_id,
        user_guid),
    PRIMARY KEY (origin_id, user_guid, connectedto_user_guid)
);

COMMENT ON COLUMN interlinkage. "_user_connectsto_user".user_guid IS 'A user
 that is connected to user y (e.g. this user x is the follower of
 user y). Being connected to someone (e.g. following someone) is only a
 mutual relationship if both users connect to each other (in this case,
 they become friends).';

COMMENT ON COLUMN interlinkage. "_user_connectsto_user".connectedto_user_guid IS 'A
 user y.';


/** Create relational table _user_memberof_group
 */
CREATE TABLE interlinkage. "_user_memberof_group" (
    origin_id int REFERENCES social. "origin" (origin_id),
    user_guid text,
    FOREIGN KEY (origin_id,
        user_guid) REFERENCES social. "user" (origin_id,
        user_guid),
    group_guid text,
    FOREIGN KEY (origin_id,
        group_guid) REFERENCES social. "user_groups" (origin_id,
        usergroup_guid),
    PRIMARY KEY (origin_id, user_guid, group_guid)
);

COMMENT ON COLUMN interlinkage. "_user_memberof_group".user_guid IS 'A user x.';

COMMENT ON COLUMN interlinkage. "_user_memberof_group".group_guid IS 'A
group where user x is member of. Membership requires active participation/
explicit sign-up  (e.g. being a member of a list on Twitter).';


/** Create relational table _user_follows_group
 */
CREATE TABLE interlinkage. "_user_follows_group" (
    origin_id int REFERENCES social. "origin" (origin_id),
    user_guid text,
    FOREIGN KEY (origin_id,
        user_guid) REFERENCES social. "user" (origin_id,
        user_guid),
    group_guid text,
    FOREIGN KEY (origin_id,
        group_guid) REFERENCES social. "user_groups" (origin_id,
        usergroup_guid),
    PRIMARY KEY (origin_id, user_guid, group_guid)
);

COMMENT ON COLUMN interlinkage. "_user_follows_group".user_guid IS 'A user x.';

COMMENT ON COLUMN interlinkage. "_user_follows_group".group_guid IS 'A group that
 user x follows. Following a group means that the user does not
  actively participate (e.g. being subscribed to a list on Twitter).';


/** Create relational table _user_mentions_user
 */
CREATE TABLE interlinkage. "_user_mentions_user" (
    origin_id int REFERENCES social. "origin" (origin_id),
    user_guid text,
    FOREIGN KEY (origin_id,
        user_guid) REFERENCES social. "user" (origin_id,
        user_guid),
    mentioneduser_guid text,
    FOREIGN KEY (origin_id,
        mentioneduser_guid) REFERENCES social. "user" (origin_id,
        user_guid),
    PRIMARY KEY (origin_id, user_guid, mentioneduser_guid)
);

COMMENT ON COLUMN interlinkage. "_user_mentions_user".user_guid IS 'A user x.';

COMMENT ON COLUMN interlinkage. "_user_mentions_user".mentioneduser_guid IS 'A user
 y that was mentioned by user x (e.g. @-Mentions in posts on Facebook,
 Twitter, Instagram etc.).';


/** Create relational table _post_taggedwith_term
 */
CREATE TABLE interlinkage. "_post_taggedwith_term" (
    origin_id int REFERENCES social. "origin" (origin_id),
    post_guid text,
    FOREIGN KEY (origin_id,
        post_guid) REFERENCES topical. "post" (origin_id,
        post_guid),
    term text,
    FOREIGN KEY (origin_id,
        term) REFERENCES topical. "term" (origin_id,
        term),
    PRIMARY KEY (origin_id, post_guid, term)
);

COMMENT ON COLUMN interlinkage. "_post_taggedwith_term".post_guid IS 'A post x.';

COMMENT ON COLUMN interlinkage. "_post_taggedwith_term".term IS 'A term
 that is assigned as a (hash-)tag to post x.';


/** Create relational table _post_term
 */
CREATE TABLE interlinkage. "_post_term" (
    origin_id int REFERENCES social. "origin" (origin_id),
    post_guid text,
    FOREIGN KEY (origin_id,
        post_guid) REFERENCES topical. "post" (origin_id,
        post_guid),
    term text,
    FOREIGN KEY (origin_id,
        term) REFERENCES topical. "term" (origin_id,
        term),
    PRIMARY KEY (origin_id, post_guid, term)
);

COMMENT ON COLUMN interlinkage. "_post_term".post_guid IS 'A post x.';

COMMENT ON COLUMN interlinkage. "_post_term".term IS 'A term
 that is used in post x (except for hashtags, see relationship above).';


/** Create relational table __users_shareidentity
 */
CREATE TABLE interlinkage. "__users_shareidentity" (
    origin_id int REFERENCES social. "origin" (origin_id),
    user_guid text,
    FOREIGN KEY (origin_id,
        user_guid) REFERENCES social. "user" (origin_id,
        user_guid),
    origin_id_rel int REFERENCES social. "origin" (origin_id),
    user_guid_rel text,
    FOREIGN KEY (origin_id_rel,
        user_guid_rel) REFERENCES social. "user" (origin_id,
        user_guid),
    PRIMARY KEY (origin_id, user_guid, origin_id_rel, user_guid_rel)
);

COMMENT ON COLUMN interlinkage. "__users_shareidentity".user_guid IS 'A user.';

COMMENT ON COLUMN interlinkage. "__users_shareidentity".user_guid_rel IS 'The
 related user with different origin_id (e.g. this user with origin_id x is
 the same as user with origin_id y). One user can be active
 on multiple origin IDs (e.g. retweet Instagram posts).';


/** Create relational table __posts_shareidentity
 */
CREATE TABLE interlinkage. "__posts_shareidentity" (
    origin_id int REFERENCES social. "origin" (origin_id),
    post_guid text,
    FOREIGN KEY (origin_id,
        post_guid) REFERENCES topical. "post" (origin_id,
        post_guid),
    origin_id_rel int REFERENCES social. "origin" (origin_id),
    post_guid_rel text,
    FOREIGN KEY (origin_id_rel,
        post_guid_rel) REFERENCES topical. "post" (origin_id,
        post_guid),
    PRIMARY KEY (origin_id, post_guid, origin_id_rel, post_guid_rel)
);

COMMENT ON COLUMN interlinkage. "__posts_shareidentity".post_guid IS 'A post.';

COMMENT ON COLUMN interlinkage. "__posts_shareidentity".post_guid_rel IS 'The same
 post with different origin_id (e.g. this post with origin_id x is the same
 as post with origin_id y). One user can publish a post on multiple origin
 IDs (e.g. automatically retweet Instagram posts).';


/** Create relational table __places_shareidentity
 */
CREATE TABLE interlinkage. "__places_shareidentity" (
    origin_id int REFERENCES social. "origin" (origin_id),
    place_guid text,
    FOREIGN KEY (origin_id,
        place_guid) REFERENCES spatial. "place" (origin_id,
        place_guid),
    origin_id_rel int REFERENCES social. "origin" (origin_id),
    place_guid_rel text,
    FOREIGN KEY (origin_id_rel,
        place_guid_rel) REFERENCES spatial. "place" (origin_id,
        place_guid),
    PRIMARY KEY (origin_id, place_guid, origin_id_rel, place_guid_rel)
);

COMMENT ON COLUMN interlinkage. "__places_shareidentity".place_guid IS 'A place.';

COMMENT ON COLUMN interlinkage. "__places_shareidentity".place_guid_rel IS 'The
 same place with different origin_id (e.g. this place with origin_id x is
 the same as place with origin_id y). One place can exist on multiple
 origin IDs, with different ids.';


/** Create relational table ___place_match_osm
 */
CREATE TABLE interlinkage. "___place_match_osm" (
    origin_id int REFERENCES social. "origin" (origin_id),
    place_guid text,
    FOREIGN KEY (origin_id,
        place_guid) REFERENCES spatial. "place" (origin_id,
        place_guid),
    osm_guid text,
    osm_name text,
    matching_similarity real,
    PRIMARY KEY (origin_id, place_guid, osm_guid)
);

COMMENT ON COLUMN interlinkage. "___place_match_osm".place_guid IS 'A place
 (LBSN).';

COMMENT ON COLUMN interlinkage. "___place_match_osm".osm_guid IS 'A matching
 entity on OpenStreetmap (e.g. this user-contributed place from social
 media matches a polygon or point volunteered on OpenStreetMap).';

COMMENT ON COLUMN interlinkage. "___place_match_osm".osm_name IS 'The entity
 name from OSM.';

COMMENT ON COLUMN interlinkage. "___place_match_osm".matching_similarity IS 'A
 similarity measure expressing certainty between 0 and 1
 (0 = very uncertain, 1 = very certain).';


/** Create relational table _user_friends_user.
 *
 * For some relationships, the direction is important (e.g. user x mentions
 * user y), for others such as being friends, direction does not matter
 * (e.g. two users become friends, which is a mutual relationship).
 *
 * However, some services such as Twitter allow one-directional
 * connections (e.g. following someone). This is only a mutual
 * relationship if both users follow each other (in this case,
 * they become friends).
 *
 * Friends (existing two-way-relationships) can also be
 * selected with the following Query:
 * SELECT origin_id,user_guid,connectedto_user_guid
 * FROM social._user_connectsto_user INTERSECT
 * SELECT origin_id,connectedto_user_guid,user_guid
 * FROM social._user_connectsto_user;
 *
 * Note that "friend" on Social Media can mean many things,
 * beyond the conventional use of the word.
 */
CREATE TABLE interlinkage."_user_friends_user" (
    origin_id int REFERENCES social."origin"(origin_id),
    user_guid text,
    FOREIGN KEY (origin_id, user_guid) REFERENCES social."user"(
    origin_id, user_guid),
    friend_guid text,
    FOREIGN KEY (origin_id, friend_guid) REFERENCES social."user"(
    origin_id, user_guid),
    PRIMARY KEY (origin_id, user_guid, friend_guid),
    CONSTRAINT user_friend_directionless
    FOREIGN KEY (origin_id, friend_guid, user_guid)
    REFERENCES interlinkage."_user_friends_user"(
    origin_id, user_guid, friend_guid),
    CONSTRAINT user_friend_self_referencing_not_allowed
    CHECK (user_guid <> friend_guid)
);

COMMENT ON COLUMN interlinkage."_user_friends_user".user_guid IS 'Two users
 x and y that became friends. Being a friend is a mutual relationship
 (pair order not relevant).';
COMMENT ON COLUMN interlinkage."_user_friends_user".friend_guid IS 'Two
 users x and y that became friends. Being a friend is a mutual
 relationship (pair order not relevant).';
