/*
 * Topical Facet of the Common LBSN Data Structure
 *
 */
 
/* Create table post
 *
 * post_type: video (instagram), image (instagram, flickr), text (twitter)
 * post_body: description (flickr), caption (instagram), tweet (twitter)
 * hashtags: array of strings
 * emoji: array of strings
 * post_latlng column: only if lat/lng coordinates are available,
 * otherwise add location information as in place, city or country table
 */
CREATE TABLE topical. "post" (
    origin_id int REFERENCES social. "origin" (origin_id),
    post_guid text,
    PRIMARY KEY (origin_id, post_guid),
    post_latlng geometry(Point, 4326) NOT NULL DEFAULT ST_GeomFromText ('POINT(0 0)',
        4326),
    place_guid text,
    FOREIGN KEY (origin_id,
        place_guid) REFERENCES spatial. "place" (origin_id,
        place_guid),
    city_guid text,
    FOREIGN KEY (origin_id,
        city_guid) REFERENCES spatial. "city" (origin_id,
        city_guid),
    country_guid text,
    FOREIGN KEY (origin_id,
        country_guid) REFERENCES spatial. "country" (origin_id,
        country_guid),
    user_guid text,
    FOREIGN KEY (origin_id,
        user_guid) REFERENCES social. "user" (origin_id,
        user_guid),
    post_publish_date timestamp,
    post_body text,
    post_geoaccuracy text DEFAULT 'unknown',
    CONSTRAINT valid_post_geoaccuracy CHECK (post_geoaccuracy IN ('unknown',
            'latlng',
            'place',
            'city',
            'country')), -- allow no other values
    hashtags text[], --
    emoji text[],
    post_like_count bigint,
    post_comment_count bigint,
    post_views_count bigint,
    post_title text,
    post_create_date timestamp,
    post_thumbnail_url text,
    post_url text,
    post_type text DEFAULT 'text',
    post_filter text,
    post_quote_count bigint,
    post_share_count bigint,
    post_language text REFERENCES social. "language" (language_short),
    input_source text,
    user_mentions text[],
    modified timestamp DEFAULT transaction_timestamp(),
    post_content_license int,
    topic_group text[],
    post_downvotes bigint
)
WITH (
    fillfactor = 95
);

COMMENT ON COLUMN topical. "post".post_latlng IS 'Coordinates of post
 with highest locational accuracy, either exact location or
 substituted from place, city or country table.';

COMMENT ON COLUMN topical. "post".post_geoaccuracy IS 'Specifies highest
 location accuracy available, either "latlng", "place", "city" or "country".';

COMMENT ON COLUMN topical. "post".input_source IS 'Type of input
 device used by the user to post, for a list see Twitter, e.g. "Web",
 "IPhone", "Android" etc., recommendation: should be oriented at
 Twitter source types.';

COMMENT ON COLUMN topical. "post".hashtags IS 'User given (hash)tags
 now stored in array.';

COMMENT ON COLUMN topical. "post".post_comment_count IS 'Number of
 times this Post has been commented by other users, e.g. count of
 Reply-Tweets in Twitter, Count of Comments in Flickr etc.';

COMMENT ON COLUMN topical. "post".post_quote_count IS 'Number of times
 this Post has been quoted by other users, e.g. count of Quote-Tweets
 in Twitter.';

COMMENT ON COLUMN topical. "post".post_share_count IS 'Number of times
 this Post has been shared by other users, e.g. count of Retweets in Twitter.';

COMMENT ON COLUMN topical. "post".post_language IS 'A BCP 47 language
 identifier corresponding to the machine-detected language of the
 Post body-text, empty/whitespace if no language could be detected,
 NULL if not specified.';

COMMENT ON COLUMN topical. "post".user_mentions IS 'An array consisting
 of user_guids that are mentioned in the post_body, post_title or other
 parts of a post. Note that arrays do not contain direct foreign keys
 checked by postgres.';

COMMENT ON COLUMN topical. "post".modified IS 'The latest timestamp
 this rows was updated (defaults to the time of insertion/update)';

COMMENT ON COLUMN topical. "post".post_content_license IS 'An integer
 for specifying licenses attached to post (e.g. All Rights Reserved = 0).
 Number scheme convention may be used from Flickr:
 https://www.flickr.com/services/api/flickr.photos.licenses.getInfo.html';


/* Create table post_reaction
 *
 * post_reaction is a reduced structure for post.
 * For mapping the spread of information,
 * it contains two rows for referencing original post
 * that motivated the reaction (referencedPost_guid)
 * or a reference to another reaction that was reacted
 * upon (referencedPostreaction_guid)
 * Example reaction_types: share, reply, quote,
 * like/star/highlight, emoji etc.
 */
CREATE TABLE topical. "post_reaction" (
    origin_id int REFERENCES social. "origin" (origin_id),
    reaction_guid text,
    PRIMARY KEY (origin_id, reaction_guid),
    referencedPost_guid text,
    FOREIGN KEY (origin_id,
        referencedPost_guid) REFERENCES topical. "post" (origin_id,
        post_guid),
    user_guid text,
    FOREIGN KEY (origin_id,
        user_guid) REFERENCES social. "user" (origin_id,
        user_guid),
    reaction_latlng geometry(Point, 4326) DEFAULT ST_GeomFromText ('POINT(0 0)',
        4326),
    reaction_type text DEFAULT 'unknown',
    CONSTRAINT valid_reaction_type CHECK (reaction_type IN ('unknown',
            'share',
            'comment',
            'quote',
            'like',
            'emoji',
            'other')),
    reaction_date timestamp,
    reaction_content text,
    reaction_like_count bigint,
    user_mentions text[],
    referencedPostreaction_guid text,
    FOREIGN KEY (origin_id,
        referencedPostreaction_guid) REFERENCES topical. "post_reaction" (origin_id,
        reaction_guid),
    modified timestamp DEFAULT transaction_timestamp()
);

COMMENT ON COLUMN topical. "post_reaction".referencedPost_guid IS 'A reference
 to post to which this reaction refers to  (e.g. doe a reaction of type REPLY,
 reference original post_pkey here).';

COMMENT ON COLUMN topical. "post_reaction".referencedPostreaction_guid IS 'A
 reference to another reaction (e.g. if this reaction is a "like" of another
reaction, reference original postreaction_pkey here).';

COMMENT ON COLUMN topical. "post_reaction".reaction_type IS 'Choose the more
 specific type if multiple apply. Merge similar types:
 Retweet -> Share; Star -> Like.';

COMMENT ON COLUMN topical. "post_reaction".modified IS 'The latest
 timestamp this rows was updated (defaults to the time of insertion)';


/* Create table term
 *
 * A term can be a hashtag or any other word mentioned by a
 * user in a post. Posts and terms are assigned through
 * relationship tables _post_hashtag_term and _post_term.
 */
CREATE TABLE topical. "term" (
    origin_id int REFERENCES social. "origin" (origin_id),
    term text,
    PRIMARY KEY (origin_id, term),
    term_post_count bigint
);