/* Delete default schemas
 */
DROP SCHEMA IF EXISTS public CASCADE;

/* Delete custom schemas
 */
DROP SCHEMA IF EXISTS social CASCADE;
DROP SCHEMA IF EXISTS spatial CASCADE;
DROP SCHEMA IF EXISTS topical CASCADE;
DROP SCHEMA IF EXISTS temporal CASCADE;
DROP SCHEMA IF EXISTS interlinkage CASCADE;

/* Create custom schemas
 */
CREATE SCHEMA social;
CREATE SCHEMA spatial;
CREATE SCHEMA topical;
CREATE SCHEMA temporal;
CREATE SCHEMA interlinkage;

/* Grant access rights
 */
GRANT ALL ON SCHEMA social TO postgres, public;
GRANT ALL ON SCHEMA spatial TO postgres, public;
GRANT ALL ON SCHEMA topical TO postgres, public;
GRANT ALL ON SCHEMA temporal TO postgres, public;
GRANT ALL ON SCHEMA interlinkage TO postgres, public;

/* Add schemas to search path
 */
 ALTER DATABASE :DBNAME SET search_path = "$user", social, spatial, temporal, topical, interlinkage, extensions;

