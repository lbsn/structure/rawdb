/*
 * Temporal Facet of the Common LBSN Data Structure
 *
 */
 
/* An event with a representation on LBSN.
 *
 * Events are temporal reference points with a start and end date.
 * Start and end date may coincide.
 */
 
/* Create table event
 */
CREATE TABLE temporal. "event" (
    event_guid text,
    origin_id int REFERENCES social. "origin" (origin_id),
    PRIMARY KEY (origin_id, event_guid),
    name text,
    event_latlng geometry(Point, 4326),
    event_area geometry(Polygon, 4326),
    event_website text,
    event_date timestamp,
    event_date_start timestamp,
    event_date_end timestamp,
    duration interval,
    place_guid text,
    FOREIGN KEY (origin_id,
        place_guid) REFERENCES spatial. "place" (origin_id,
        place_guid),
    city_guid text,
    FOREIGN KEY (origin_id,
        city_guid) REFERENCES spatial. "city" (origin_id,
        city_guid),
    country_guid text,
    FOREIGN KEY (origin_id,
        country_guid) REFERENCES spatial. "country" (origin_id,
        country_guid),
    user_guid text,
    FOREIGN KEY (origin_id,
        user_guid) REFERENCES social. "user" (origin_id,
        user_guid),
    event_description text,
    event_type text,
    event_share_count bigint,
    event_like_count bigint,
    event_comment_count bigint,
    event_views_count bigint,
    event_engage_count bigint
);

COMMENT ON COLUMN temporal. "event".name IS 'Name of the event.';

COMMENT ON COLUMN temporal. "event".event_date IS 'Date and time of the event.';

COMMENT ON COLUMN temporal. "event".event_date_start IS 'Start date of the event.';

COMMENT ON COLUMN temporal. "event".event_date_end IS 'End date of the event.';

COMMENT ON COLUMN temporal. "event".duration IS 'Duration of the event in seconds.';

COMMENT ON COLUMN temporal. "event".event_latlng IS 'Location of the event (WKT Point).';

COMMENT ON COLUMN temporal. "event".event_area IS 'Location of the event (WKT Polygon).';

COMMENT ON COLUMN temporal. "event".place_guid IS 'Place reference.';

COMMENT ON COLUMN temporal. "event".city_guid IS 'City reference.';

COMMENT ON COLUMN temporal. "event".country_guid IS 'Country reference.';

COMMENT ON COLUMN temporal. "event".user_guid IS 'User reference (e.g. the owner of the event).';

COMMENT ON COLUMN temporal. "event".event_description IS 'A description of the event.';

COMMENT ON COLUMN temporal. "event".event_website IS 'Url to the public website of the event.';

COMMENT ON COLUMN temporal. "event".event_type IS 'Any string to describe the type of event.';

COMMENT ON COLUMN temporal. "event".event_share_count IS 'Number of times this Event has been 
 shared by other users.';

COMMENT ON COLUMN temporal. "event".event_like_count IS 'Number of times this Event has been
 liked or highlighted by other users.';

COMMENT ON COLUMN temporal. "event".event_comment_count IS 'Number of times this Event has been
 commented on.';

COMMENT ON COLUMN temporal. "event".event_views_count IS 'Number of times this Event has been
 viewed.';

COMMENT ON COLUMN temporal. "event".event_engage_count IS 'Number of users who participate in this Event.';