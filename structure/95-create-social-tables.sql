/*
 * Social Facet of the Common LBSN Data Structure
 *
 */
 
/* Create table origin
 */
CREATE TABLE social. "origin" (
    origin_id serial PRIMARY KEY,
    name text UNIQUE
);

COMMENT ON COLUMN social. "origin".origin_id IS 'Number of LBSN source';

COMMENT ON COLUMN social. "origin".name IS 'Full name of LBSN source';


/* Create table langauge
 */
CREATE TABLE social. "language" (
    language_short text PRIMARY KEY,
    language_name text,
    language_name_de text
);

COMMENT ON COLUMN social. "language".language_short IS 'The language
 code may be formatted as ISO 639-1 alpha-2 (en), ISO 639-3 alpha-3
 (msa), or ISO 639-1 alpha-2 combined with an ISO 3166-1 alpha-2
 localization (zh-tw).';

COMMENT ON COLUMN social. "language".language_name IS 'Full language name';

COMMENT ON COLUMN social. "language".language_name_de IS 'Full german
 language name';


/* Create table user
 */
CREATE TABLE social. "user" (
    origin_id int REFERENCES social. "origin" (origin_id),
    user_guid text,
    PRIMARY KEY (origin_id, user_guid),
    user_name text,
    user_fullname text,
    follows bigint,
    followed bigint,
    biography text,
    post_count bigint,
    url text,
    is_private boolean,
    is_available boolean,
    user_language text REFERENCES social. "language" (language_short),
    user_location text,
    user_location_geom geometry(Point, 4326),
    liked_count bigint,
    active_since timestamp,
    profile_image_url text,
    user_timezone text,
    user_utc_offset int,
    user_groups_member text[],
    user_groups_follows text[],
    group_count bigint,
    modified timestamp DEFAULT transaction_timestamp()
)
WITH (
    fillfactor = 80
);

COMMENT ON COLUMN social. "user".group_count IS 'The number
 of public groups or communities this user is part of.';

COMMENT ON COLUMN social. "user".user_location IS 'The user-defined
 location for this profile. Not necessarily a location,
 nor machine-parseable.';

COMMENT ON COLUMN social. "user".user_location_geom IS 'Coordinates
 (lat/lng) of the user-location, either provided by user or
  geocoded from user_location_name.';

COMMENT ON COLUMN social. "user".liked_count IS 'The number of
 posts this user has liked in total.';

COMMENT ON COLUMN social. "user".active_since IS 'UTC datetime when
 the user was first active (e.g. time of account creation,
 or derived from first post_publish_date)';

COMMENT ON COLUMN social. "user".profile_image_url IS 'URL pointing
 to the profile image of the user.';

COMMENT ON COLUMN social. "user".user_groups_member IS 'The list of
 groups this user has joined/ is a member of (active participation interest).';

COMMENT ON COLUMN social. "user".user_groups_follows IS 'The list
 of groups this user follows (viewing interest).';

COMMENT ON COLUMN social. "user".modified IS 'The latest timestamp
 this rows was updated (defaults to the time of insertion)';


/* Create table user_groups
 */
CREATE TABLE social. "user_groups" (
    origin_id int REFERENCES social. "origin" (origin_id),
    usergroup_guid text,
    PRIMARY KEY (origin_id, usergroup_guid),
    usergroup_name text,
    usergroup_description text,
    member_count bigint,
    usergroup_createdate timestamp,
    user_owner text,
    FOREIGN KEY (origin_id,
        user_owner) REFERENCES social. "user" (origin_id,
        user_guid)
);