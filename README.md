[![version](https://lbsn.vgiscience.org/structure/rawdb/version.svg)](https://gitlab.vgiscience.de/lbsn/structure/rawdb/tree/master) [![pipeline status](https://gitlab.vgiscience.de/lbsn/structure/rawdb/badges/master/pipeline.svg)](https://gitlab.vgiscience.de/lbsn/structure/rawdb/commits/master)

# LBSN STRUCTURE RAW - SQL Files

The base SQL version of the [common location based social network (LBSN) data structure concept](https://lbsn.vgiscience.org) to handle cross network Social Media data.
There are several motivations for prividing a common LBSN interchange data structure. Firstly, the [GDPR](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX%3A32016R0679) directly requests Social Media Network operators to allow users to transfer accounts and data inbetween services. 
While there are attempts by Google, Facebook etc. (see [data-transfer-project](https://github.com/google/data-transfer-project)), it is not currently possible. With this structure concept, we follow an independent road.
A primary goal is to systematically characterize LBSN data aspects in a common scheme that enables privacy-by-design for connected software, transfer scripts and database design.

This is the LBSN RAW version, described in SQL. These SQL files can be used to set up LBSN Structure in Postgres from scratch. If you already initialized LBSN Structure and want to upgrade to the newest version, have a look at the [LBSN Alembic Migrations](https://gitlab.vgiscience.de/lbsn/theplink-docker/theplink-docker-app/tree/master/alembic) repository.

A complete guide is provided in the [LBSN documentation](https://lbsn.vgiscience.org)