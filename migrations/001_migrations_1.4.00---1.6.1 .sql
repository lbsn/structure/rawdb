/*
 * Schema migrations for upgrading from 
 * LBSN Structure (raw) version 1.4.00 to 1.6.2
 *
 * Notes:
 * - Generated with [migra][migra]
 * - CAP style: Manual overrides of migra sql
 *
 * migra: https://databaseci.com/docs/migra
 */

/* Add new attributes to topical.Post
 */
ALTER TABLE "topical"."post"
    ADD COLUMN topic_group text[];

ALTER TABLE "topical"."post"
    ADD COLUMN post_downvotes bigint;
