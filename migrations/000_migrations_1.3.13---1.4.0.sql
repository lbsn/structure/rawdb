/*
 * Schema migrations for upgrading from 
 * LBSN Structure (raw) version 1.3.13 to 1.4.0
 *
 * Notes:
 * - Generated with [migra][migra]
 * - CAP style: Manual overrides of migra sql
 *
 * migra: https://databaseci.com/docs/migra
 */
 
create schema if not exists "interlinkage";

/* Add schemas to search path
 */
ALTER DATABASE "rawdb" SET search_path TO "$user", social, spatial, temporal, topical, interlinkage, extensions;
 
GRANT ALL ON SCHEMA interlinkage TO postgres, public;

alter extension "postgis" update to '2.5.5';

ALTER TABLE "social"."__users_shareidentity"
    SET SCHEMA "interlinkage";

ALTER TABLE "social"."_user_connectsto_user"
    SET SCHEMA "interlinkage";

ALTER TABLE "social"."_user_follows_group"
    SET SCHEMA "interlinkage";
    
ALTER TABLE "social"."_user_memberof_group"
    SET SCHEMA "interlinkage";

ALTER TABLE "social"."_user_mentions_user"
    SET SCHEMA "interlinkage";
    
ALTER TABLE "spatial"."___place_match_osm"
    SET SCHEMA "interlinkage";

ALTER TABLE "spatial"."__places_shareidentity"
    SET SCHEMA "interlinkage";
    
ALTER TABLE "topical"."__posts_shareidentity"
    SET SCHEMA "interlinkage";

ALTER TABLE "topical"."_post_taggedwith_term"
    SET SCHEMA "interlinkage";
    
ALTER TABLE "topical"."_post_term"
    SET SCHEMA "interlinkage";

/* Create table event
 */
CREATE TABLE temporal. "event" (
    event_guid text,
    origin_id int REFERENCES social. "origin" (origin_id),
    PRIMARY KEY (origin_id, event_guid),
    name text,
    event_latlng geometry(Point, 4326),
    event_area geometry(Polygon, 4326),
    event_website text,
    event_date timestamp,
    event_date_start timestamp,
    event_date_end timestamp,
    duration interval,
    place_guid text,
    FOREIGN KEY (origin_id,
        place_guid) REFERENCES spatial. "place" (origin_id,
        place_guid),
    city_guid text,
    FOREIGN KEY (origin_id,
        city_guid) REFERENCES spatial. "city" (origin_id,
        city_guid),
    country_guid text,
    FOREIGN KEY (origin_id,
        country_guid) REFERENCES spatial. "country" (origin_id,
        country_guid),
    user_guid text,
    FOREIGN KEY (origin_id,
        user_guid) REFERENCES social. "user" (origin_id,
        user_guid),
    event_description text,
    event_type text,
    event_share_count bigint,
    event_like_count bigint,
    event_comment_count bigint,
    event_views_count bigint,
    event_engage_count bigint
);

COMMENT ON COLUMN temporal. "event".name IS 'Name of the event.';

COMMENT ON COLUMN temporal. "event".event_date IS 'Date and time of the event.';

COMMENT ON COLUMN temporal. "event".event_date_start IS 'Start date of the event.';

COMMENT ON COLUMN temporal. "event".event_date_end IS 'End date of the event.';

COMMENT ON COLUMN temporal. "event".duration IS 'Duration of the event in seconds.';

COMMENT ON COLUMN temporal. "event".event_latlng IS 'Location of the event (WKT Point).';

COMMENT ON COLUMN temporal. "event".event_area IS 'Location of the event (WKT Polygon).';

COMMENT ON COLUMN temporal. "event".place_guid IS 'Place reference.';

COMMENT ON COLUMN temporal. "event".city_guid IS 'City reference.';

COMMENT ON COLUMN temporal. "event".country_guid IS 'Country reference.';

COMMENT ON COLUMN temporal. "event".user_guid IS 'User reference (e.g. the owner of the event).';

COMMENT ON COLUMN temporal. "event".event_description IS 'A description of the event.';

COMMENT ON COLUMN temporal. "event".event_website IS 'Url to the public website of the event.';

COMMENT ON COLUMN temporal. "event".event_type IS 'Any string to describe the type of event.';

COMMENT ON COLUMN temporal. "event".event_share_count IS 'Number of times this Event has been 
 shared by other users.';

COMMENT ON COLUMN temporal. "event".event_like_count IS 'Number of times this Event has been
 liked or highlighted by other users.';

COMMENT ON COLUMN temporal. "event".event_comment_count IS 'Number of times this Event has been
 commented on.';

COMMENT ON COLUMN temporal. "event".event_views_count IS 'Number of times this Event has been
 viewed.';

COMMENT ON COLUMN temporal. "event".event_engage_count IS 'Number of users who participate in this Event.';

/* Produce pseudonymized hash of input id with skey
 * - using skey as key
 * - sha256 cryptographic hash function
 * - encode in base64 to reduce length of hash
 * - remove trailing '=' from base64 string
 * - return as text
 */
CREATE OR REPLACE FUNCTION 
extensions.crypt_hash (id text, skey text)
RETURNS text
AS $$
    SELECT 
        RTRIM(
            ENCODE(
                HMAC(
                    id::bytea,
                    skey::bytea,
                    'sha256'), 
                'base64'),
            '=')
$$
LANGUAGE SQL
STRICT;
