# Migrations

The migrations provided here can be used as a base for   
migrating live servers that contain already data.  

These migrations are provided without warranty: Use with caution.  

Migrations created with [migra][migra], verified
and optionally modified manually.

Migra script example:

```bash
migra postgresql://postgres:{POSTGRES_PASSWORD}@127.0.0.1:15432/rawdb \
      postgresql://postgres:{POSTGRES_PASSWORD}@127.0.0.1:15433/rawdb
```

[migra]: https://databaseci.com/docs/migra